﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ExitPointer : MonoBehaviour
{
    public GameObject player; // john lemon
    public GameEnding gameEnding; // exit door
    // Start is called before the first frame update
    public float offset;

    private Vector3 playerPosition;
    private Vector3 targetPosition;
    private Vector3 direction;
    private float angle;
    private RectTransform pointerRectTransform; // the arrow pointing to the exit
    void Start()
    {
        targetPosition = new Vector3(18, 1, 1.5f);
        pointerRectTransform = GetComponent<RectTransform>();
        angle = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 toPosition = targetPosition;
        Vector3 fromPosition = player.transform.position;
        //Vector3 fromPosition = Camera.main.transform.position;
        Vector3 dir = (toPosition - fromPosition).normalized; // need to normalize before we find the angle
        float angle = Vector3.Angle(dir, Vector3.forward); // angle is calculated, most likely the dot product is used under the hood
        // Since angle returns an unsigned int and the exit is on the right of our player we can negate the angle to make it point to the door
        pointerRectTransform.eulerAngles = new Vector3(0, 0, -(angle)); 
    }
}
