﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f;
    Animator m_Animator;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity; // Quaternion is a way of storing rotations
    Rigidbody m_rigidbody;
    AudioSource m_AudioSource;
 
    public GameObject endGame;


    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();
 
    }
 
    // FixedUpdate is called before the physics system solves any collisions and other interactions that have happened
    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        // checking for user input to see if player should be walking
        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);
        if (isWalking)
        {
            if(!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
            else
            {
                m_AudioSource.Stop();
            }
        }
        else
        {

        }
        // updating turn speed
        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward); 

    }
    // applies movement not rotation
    void OnAnimatorMove()
    {
        m_rigidbody.MovePosition(m_rigidbody.position + (m_Movement * m_Animator.deltaPosition.magnitude));
        m_rigidbody.MoveRotation(m_Rotation);
    }

}
