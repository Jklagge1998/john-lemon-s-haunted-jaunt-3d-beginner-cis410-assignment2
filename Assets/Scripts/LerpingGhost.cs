﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpingGhost : MonoBehaviour
{
    [SerializeField] [Range(0f, 4f)] float lerptime;
    public Vector3[] positions;
    int posIndex;
    int length;

    float t;

    bool pass;

    

    // Start is called before the first frame update
    void Start()
    {
        posIndex = 0;
        t = 0f;
        length = positions.Length;
        pass = false;
    }

    // Update is called once per frame
    void Update()
    {
        // Ghost's position is change with lerping function
        transform.position = Vector3.Lerp(transform.position, positions[posIndex], lerptime * Time.deltaTime);
        t = Mathf.Lerp(t, 1f, lerptime * Time.deltaTime);
        if (t > .9f)
        {
            if (pass == false)
            {
                t = 0f;
                posIndex++;
                if (posIndex == length)
                {
                    // we have reached the end of the ghosts current path
                    pass = true; // ghost will now turn back
                    posIndex--;
                    // make ghost turn around
                    transform.Rotate(new Vector3(transform.rotation.x, transform.rotation.y + 180, transform.rotation.z));
                }
            }
            else
            {
                // same logic as above, but with for the oppsite end of the path.
                t = 0f;
                posIndex--;
                if (posIndex == -1)
                {
                    pass = false;
                    posIndex++;
                    transform.Rotate(new Vector3(transform.rotation.x, transform.rotation.y + 180, transform.rotation.z));
                }
            }
            
            //posIndex = (posIndex >= length) ? 0 : posIndex;
        }
        
    }

}
