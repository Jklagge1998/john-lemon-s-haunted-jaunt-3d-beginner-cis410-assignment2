﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Notes: Observer will be used for enemies point of view to detect the player
public class Observer : MonoBehaviour
{
    public Transform player;
    public GameEnding gameEnding;

    bool m_IsPlayerInRange;
    
    void OnTriggerEnter(Collider other)
    {
        // Checks if the player has entered the objects collider
        if (other.transform == player)
        {
            m_IsPlayerInRange = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        // Checks if the player has left the objects collider
        if (other.transform == player)
        {
            m_IsPlayerInRange = false;
        }
    }

    void Update()
    {
        if (m_IsPlayerInRange)
        {
            // Note: Raycasting is like a beam that is sent in a direction
            Vector3 direction = player.position - transform.position + Vector3.up;
            Ray ray = new Ray(transform.position, direction);
            RaycastHit raycastHit; // an object who's collider was found in the raycast (if nothing is found it is null

            if (Physics.Raycast(ray, out raycastHit))
            {
                if (raycastHit.transform == player)
                {
                    // player has been found in point of the view of the enemy and has been caught
                    gameEnding.CaughtPlayer(); // level will restart
                }
            }
        }
        
    }
}
