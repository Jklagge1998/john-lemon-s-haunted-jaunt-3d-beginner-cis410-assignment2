﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEnding : MonoBehaviour
{
    public float fadeDuration = 1f;
    public float displayImageDuration = 1f;
    public GameObject player;
    public CanvasGroup exitBackgroundImageCanvasGroup;
    public CanvasGroup caughtBackgroundImageCanvasGroup;
    public AudioSource exitAudio;
    public AudioSource caughtAudio;
        

    bool m_IsPlayerAtExit;
    bool m_IsPlayerCaught;
    float m_timer;
    bool m_HasAudioPlayed;

    private void OnTriggerEnter(Collider other)
    {
        // if player enters the trigger
        if (other.gameObject == player)
        {
            m_IsPlayerAtExit = true; // player has reached the exit
        }
    }

    public void CaughtPlayer()
    {
        m_IsPlayerCaught = true;
    }
    void Update()
    {
        if (m_IsPlayerAtExit)
        {
            // player has won, so the winning image will be presented and the game will end
            EndLevel(exitBackgroundImageCanvasGroup, false, exitAudio);
        }
        else if (m_IsPlayerCaught)
        {
            // player has been caught, so the caught image will be presented and the game will restarted (scene will be reloaded)
            EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio);
        }
        
    }
    
    void EndLevel(CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource)
    {
        if (!m_HasAudioPlayed)
        {
            // If audio hasn't played before audioSource will play
            audioSource.Play();
            m_HasAudioPlayed = true;
        }
        m_timer += Time.deltaTime;
        imageCanvasGroup.alpha = m_timer / fadeDuration; // the fade rate of our canvas images
        if (m_timer > fadeDuration + displayImageDuration)
        {
            if (doRestart)
            {
                SceneManager.LoadScene(0); // will restart scene
            }
            else
            {
                Application.Quit(); // ends application
            }
        }
    }
}
