# John Lemon's Haunted Jaunt #

John Lemon's Haunted Jaunt is a 3D beginner tutorial for unity. It is also the second assignment for the UO introudction to video game programming course.

### Summary ###

* Completed the John Lemon's Haunted Jaunt Unity Tutorial
* To see this tutorial click the link: [https://learn.unity.com/project/john-lemon-s-haunted-jaunt-3d-beginner](https://learn.unity.com/project/john-lemon-s-haunted-jaunt-3d-beginner)
* This was built and ran on Unity version 2019.1.14f

### Skills applied in Project ###

* Turning character Models into Prefabs
* Scripting Character Movement and using animator controllers
* Basic Enviromental lighting and setting up NavMeshes
* Setting up a player Camera using CineMachine
* Setting up Endgame conditions with CanvasGroups
* Setting up Static enemies with triggers to catch the player if they are in their sights.
* Setting up Dynamic enemies using triggers and setting up waypoints for the navMesh.
* Using Audioclips and Audiosources to setup sound for player footsteps and ghost sounds.
* Creating an audio listener for the player.

### Additonal Gameplay features###

* Implemented an arrow sprite in the bottom right corner of the UI canvas. It uses the Vector3.Angle method to calculate the angle of the exit door and the player. According to that angle the sprite arrow points to the door, giving the player a hint as to where they need to go (Note: Angle gives an unsigned float so since the player is left of the door and rotation in unity is counter-clockwise my code negated the angle). Code for this functionality can be found in the ExitPointer.cs script.
* Implemented the navigation of a ghost using lerping. This ghost has no collider and therefore can walkthrough walls. This ghost doesn't use a navigation map. It instead uses linear interpolation and an array of vector positions to get the ghost to where it needs to go.
* Added basic particle effects to replicate fog and shower steam. When adventuring through the level the player will see the particle system in room of the showering ghost and at the end of the level, just past the exit door.

### Have any questions? ###

* Email me @ Jklagge@uoregon.edu
